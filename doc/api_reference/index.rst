..
    :copyright: Copyright (c) 2017 ftrack

.. _api_reference:

*************
API reference
*************

ftrack_action_handler
===============================


.. autoclass:: ftrack_action_handler.action.BaseAction
