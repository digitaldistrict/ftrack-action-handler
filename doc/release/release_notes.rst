..
    :copyright: Copyright (c) 2014 ftrack

.. _release/release_notes:

*************
Release Notes
*************

.. release:: 0.1.3
    .. change:: add
        :tags: API

        Add session property for easier access.

.. release:: 0.1.2
    .. change:: fixed
        :tags: Action

        Variants not handled correctly.

.. release:: 0.1.0
    .. change:: new
        :tags: Action, Event, API

        Base class for simplifying the creation of new actions and working with the new
        API.


